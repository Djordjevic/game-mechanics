﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrushScript : MonoBehaviour {

    public LevelManager levelMan;

    private void Start()
    {
        levelMan = FindObjectOfType<LevelManager>();
    }

    private void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.gameObject.tag == "Player")
        {
            levelMan.RespawnPlayer();
        }
    }
    
}
