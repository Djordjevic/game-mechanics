﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour {

    public GameObject player;
    float lerpSpeed = 50f;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        var prevPos = transform.position;

        transform.position = Vector3.Lerp(prevPos, new Vector3(player.transform.position.x, player.transform.position.y + 4f, transform.position.z), lerpSpeed);
	}
}
