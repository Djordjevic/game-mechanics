﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelManager : MonoBehaviour {

    public GameObject currentCheckpoint;
    public Text gameOver;
    public Button btn;
    public Image panel;

    public Image hp1, hp2, hp3;

    private PlayerController player;

    Scene loadedScene;
	// Use this for initialization
	void Start ()
    {
        loadedScene = SceneManager.GetActiveScene();
        player = FindObjectOfType<PlayerController>();
	}
	
	// Update is called once per frame
	void Update () {
        GameOver();

        if(player.hitPoints == 2)
        {
            hp3.gameObject.SetActive(false);
        }

        if(player.hitPoints == 1)
        {
            hp2.gameObject.SetActive(false);
        }
	}

    public void RespawnPlayer()
    {
        player.transform.position = currentCheckpoint.transform.position;
        player.hitPoints--;
    }

    public void GameOver()
    {
        if(player.hitPoints == 0)
        {
            gameOver.gameObject.SetActive(true);
            btn.gameObject.SetActive(true);
            panel.gameObject.SetActive(true);

            hp1.gameObject.SetActive(false);
        }
    }

    public void RestartGame()
    {
        SceneManager.LoadScene(loadedScene.buildIndex);
    }
}
