﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KettelScript : MonoBehaviour {

    public GameObject waterTank;
    public GameObject timber;
    public GameObject explosionPos;
    public float steamPower;

    Vector3 waterPos;
    Vector3 waterScale;

    ParticleSystem.EmissionModule em;

    bool full = false;
	// Use this for initialization
	void Start () {
        waterPos = waterTank.transform.position;
        waterScale = waterTank.transform.localScale;
        
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (waterTank.transform.localScale.y >= 0.8f)
            full = true;
	}

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Water" && !full)
        {
            waterTank.transform.position = new Vector3(waterTank.transform.position.x, waterTank.transform.position.y + 0.0035f, waterTank.transform.position.z);
            waterTank.transform.localScale = new Vector3(waterTank.transform.localScale.x, waterTank.transform.localScale.y + 0.0035f, waterTank.transform.localScale.z);
            Destroy(collision.gameObject);
        }

        if(collision.gameObject.tag == "FireBomb" && full)
        {
            TimberBurning();
        }
    }

    void TimberBurning()
    {
        timber.GetComponent<ParticleSystem>().Play();
        em = timber.GetComponent<ParticleSystem>().emission;
        em.enabled = true;
        StartCoroutine("BurnTimer");
    }

    IEnumerator BurnTimer()
    {
        yield return new WaitForSeconds(2);
        em.enabled = false;
        waterTank.transform.position = waterPos;
        waterTank.transform.localScale = waterScale;
        
        explosionPos.SetActive(true);

        StartCoroutine("ExplosionDuration");
        full = false;
    }

    IEnumerator ExplosionDuration()
    {
        yield return new WaitForSeconds(2);
        explosionPos.SetActive(false);
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if(collision.gameObject.tag != "Player")
        collision.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(0f, steamPower);
    }
}

