﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class fireObstacleScript : MonoBehaviour {

    public GameObject _instanHold;
    GameObject _instan;
    // Use this for initialization
    ParticleSystem.EmissionModule em;
    MeshRenderer rend;
    BoxCollider2D boxColl;

    public LevelManager levelMan;

    private void Start()
    {
        levelMan = FindObjectOfType<LevelManager>();

        em = GetComponent<ParticleSystem>().emission;
        rend = GetComponent<MeshRenderer>();
        boxColl = GetComponent<BoxCollider2D>();
    }

    private void OnCollisionEnter2D(Collision2D coll)
    {
        if(coll.gameObject.tag == "Player")
        {
            levelMan.RespawnPlayer();
        }

        if(coll.gameObject.tag == "Water")
        {
            em.enabled = false;
            rend.enabled = false;
            boxColl.enabled = false;
            _instan = (GameObject)Instantiate(_instanHold, this.transform.position, this.transform.rotation);
            Destroy(coll.gameObject);
        }
    }
}
