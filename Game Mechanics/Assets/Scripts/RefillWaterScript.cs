﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RefillWaterScript : MonoBehaviour {
    GameObject player;
    public Slider waterBar;

    private void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
    }

    private void OnTriggerStay2D(Collider2D coll)
    {
        if (coll.gameObject == player && player.GetComponent<PlayerController>().waterActive)
        {
            if (player.GetComponent<PlayerController>().waterCharge < 100f)
            {
                player.GetComponent<PlayerController>().waterCharge++;
                waterBar.value++;
            }
            else player.GetComponent<PlayerController>().waterCharge = player.GetComponent<PlayerController>().maxWaterCharge;
        }
    }
}
