﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterController : MonoBehaviour {

    public Rigidbody2D rb;

    float startTime;
	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody2D>();
        StartCoroutine(RemoveWater());
        startTime = Time.deltaTime;
    }
    
    IEnumerator RemoveWater()
    {
        yield return new WaitForSeconds(2);
        Destroy(gameObject);
    }
}
