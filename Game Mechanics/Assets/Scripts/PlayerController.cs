﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {

    public float moveSpeed;
    public float jumpForce;

    public int hitPoints;

    public float waterSpeed;
    public float waterCharge;
    public float maxWaterCharge = 100f;
    float minWaterCharge = 0f;

    public GameObject water;
    public GameObject bomb;

    public Rigidbody2D rbPlayer;

    bool grounded = false;
    bool fired = false;

    public bool fireActive;
    public bool waterActive;

    public Slider waterBar;

    private void Start()
    {
        hitPoints = 3;
        rbPlayer = GetComponent<Rigidbody2D>();
        fireActive = false;
        waterActive = false;

        waterCharge = 0f;
    }

    private void FixedUpdate()
    {
        Movement();
        Jump();

        if (Input.GetMouseButton(0) && waterActive && waterCharge > 0f)
        {
            FireWater();
        }

        if(Input.GetMouseButtonDown(1) && !fired && fireActive)
        {
            GetComponent<ParticleSystem>().Play();
            ParticleSystem.EmissionModule em = GetComponent<ParticleSystem>().emission;
            em.enabled = true;
            FireBomb();
            StartCoroutine(FireDelay());
        }
    }

    private void Jump()
    {
        if (Input.GetKeyDown(KeyCode.W))
        {
            if (grounded)
            {
                rbPlayer.velocity = new Vector2(rbPlayer.velocity.x, jumpForce);
                grounded = false;
            }
        }
    }

    private void Movement()
    {
        float movementX = Input.GetAxisRaw("Horizontal");

        if (movementX > 0 || movementX < 0)
        {
            rbPlayer.velocity = new Vector2(moveSpeed * movementX, rbPlayer.velocity.y);
        }

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Floor")
            grounded = true;
        else grounded = false;
    }
    
    private void FireWater()
    {
        Vector2 direction = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position;
        
        GameObject waterFire = Instantiate(water, this.transform);
        waterFire.GetComponent<Rigidbody2D>().velocity = direction * 1.5f;
        waterCharge--;
        waterBar.value--;
        
    }

    private void FireBomb()
    {
        Vector2 direction = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position;

        GameObject bombFire = Instantiate(bomb, this.transform);
        bombFire.GetComponent<Rigidbody2D>().velocity = direction * 1.5f;

        fired = true;
    }

    IEnumerator FireDelay()
    {
        yield return new WaitForSeconds(1);
        fired = false;
    }
}
