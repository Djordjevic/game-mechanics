﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flamable : MonoBehaviour {

    public GameObject burningObject;

    public bool isBurning = false;

    private void Start()
    {
        
    }
    // Use this for initialization
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "FireBomb")
        {
            Burning();
        }
        
    }

    void Burning()
    {
        GetComponent<ParticleSystem>().Play();
        ParticleSystem.EmissionModule em = GetComponent<ParticleSystem>().emission;
        em.enabled = true;
        isBurning = true;
        StartCoroutine(BurnDown());
    }

    IEnumerator BurnDown()
    {
        yield return new WaitForSeconds(2);
        Destroy(gameObject);
    }
}
